#include <iostream>
using namespace std;
void main()
{
	int RES = 0;
	float NumA = 0, NumB = 0;

	cout << "Enter Num A: ";
	cin >> NumA;

	cout << "Enter Num B: ";
	cin >> NumB;

	cout << "And select operation:\n"
		<< "\t [1] - +\n"
		<< "\t [2] - -\n"
		<< "\t [3] - *\n"
		<< "\t [4] - /\n";
	cin >> RES;

	switch (RES)
	{
		case 1:
			cout << "\n " << NumA << " + " << NumB << " = " << NumA + NumB << endl;
			break; 

		case 2:
			cout << "\n " << NumA << " - " << NumB << " = " << NumA - NumB << endl;
			break; 

		case 3:
			cout << "\n " << NumA << " * " << NumB << " = " << NumA * NumB << endl;
			break;
	
		case 4:
	
			if (NumB == 0) {
				cout << "Divizion by zero!\n";
				break;
			}
			
			cout << "\n " << NumA << " / " << NumB << " = " << NumA / NumB << endl;
			break; 

		default:
			cout << "Invalid Operation.\n";
			break; 
	}
}